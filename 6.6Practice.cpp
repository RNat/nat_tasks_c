// 6.6Practice.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include <stdlib.h>
#include <time.h>
#define MAX 100

int fib2(int k, int m, int N)
{
    if (N == 1)
        return m;
    else
        return fib2(m, k + m, N - 1);
}

int fib(int N)
{
return fib2(0, 1, N);
}

int _tmain(int argc, _TCHAR* argv[])
{

    int N;
    printf("Please enter a number\n");
    scanf_s("%d", &N, MAX);
    FILE* out;
    fopen_s(&out, "output.txt", "w");

    for (int i = 1; i <= N; i++)
    {
        clock_t start = clock();
        int result = fib(i);
        clock_t end = clock();
        double time = (double)(end - start) / CLOCKS_PER_SEC;
        printf("%d - %.16f\n", result, time);
        fprintf(out, "%d,%.16f\n", result, time);
    }

    fclose(out);

    return 0;
}
