// 5.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include <conio.h>
#include <windows.h>
#define W 30
#define H 20

void Spaces(char(*arr)[W])
{
    int i, j;
    for (i = 0; i<H; i++)
        for (j = 0; j<W; j++)
            arr[i][j] = ' ';
}

void Fill(char(*arr)[W])
{
    int i, j, k;
    for (i = 0; i <= H / 2; i++)//����� ������� ����
        for (j = 0; j <= W / 2; j++)
        {
        int var = rand() % 2;
        if (var == 1)
            arr[i][j] = '*';
        }

    for (i = 0; i <= H / 2; i++) //����� ������ ����
        for (j = W / 2, k = W / 2 - 1; j<W; j++, k--)
            arr[i][j] = arr[i][k];

    for (i = H / 2, k = H / 2 - 1; i<H; i++, k--)//������ ��������
        for (j = 0; j<W; j++)
            arr[i][j] = arr[k][j];
}

void Print(char(*arr)[W])
{
    int i, j;
    for (i = 0; i<H; i++)
    {
        for (j = 0; j<W; j++)
            putchar(arr[i][j]);
        putchar('\n');
    }
}


int _tmain(int argc, _TCHAR* argv[])
{
    char arr[H][W];

    while (!_kbhit())
    {
        system("cls");
        Spaces(arr);
        Fill(arr);
        Print(arr);
        Sleep(3000);
    }

    return 0;
}