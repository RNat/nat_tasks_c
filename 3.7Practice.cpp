#include "stdafx.h"
#include <string.h>
#include <stdlib.h>
#define MAX 256

int main()
{
    char buf[MAX];
    int counts[MAX] = { 0 }, sym_number=0;

    printf("Insert the line\n");
    scanf_s("%s", buf, MAX);

    if (buf[strlen(buf) - 1] == '\n')
        buf[strlen(buf) - 1] = '\0';

    for (int i = 0; buf[i]; i++) counts[buf[i]]++;

    for (int i = 0; i < MAX; i++) if (counts[i]) sym_number++;

    for (int j = 0; j <= sym_number - 1; j++)
    {
        int max_number = 0, sym = 0, k = 0;
                while (k < MAX)
                {
                   if (counts[k] > max_number)
                   {
                     max_number = counts[k];
                      sym = k;
                   }
                   else k++;
                }

        printf("%c - %d\n", sym, max_number);
        counts[sym] = 0;
        max_number = 0;
        k = 0;
    }
    return 0;
}