#include "stdafx.h"
#include <string.h>
#include <stdlib.h>
#define MAX 256

int main()
{
    char buf[MAX];
    int counts[2*MAX] = { 0 };

    printf("Insert the line\n");
    scanf_s("%s", buf, MAX);

    if (buf[strlen(buf) - 1] == '\n')
        buf[strlen(buf) - 1] = '\0';

             for (int i = 0; buf[i]; i++)
                 counts[buf[i]]++;

                for (int i = 0; i<MAX; i++)
                    if (counts[i])
                    printf("%c - %d\n", i, counts[i]);
    return 0;
}
