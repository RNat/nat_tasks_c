// 3.5Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <time.h>
#define N 10

int _tmain(int argc, _TCHAR* argv[])
{
    int buf[N] = { 0 };
    int last=N-1;
    int first=0;
    int sum = 0;
    printf("Array is: \n");
    srand(time(NULL));

    for (int i = 0; i < N; i++)
    {
        buf[i] = rand()%20 - 10;
        printf("%d  ", buf[i]);
    }
    while (buf[first] >= 0)
        first++;
    while (buf[last]<0)
        last--;
    if (last < first)
        printf("\nAll of the negative numbers are in the second half\n");
    else
    {
        for (int i = first; i <= last; i++)
            sum+=buf[i];
        printf("\nThe sum of numbers is %d\n", sum);
    }
    return 0;
}

