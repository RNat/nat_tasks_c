// Task3Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{
	printf("Insert the angle \n");
	float res;
	float v;
	char d;
	scanf_s("%f%c", &v, &d, 1);
	while ((d != 'D') && (d != 'R'))
	{
		printf("Incorrect data! Try again\n");
		fflush(stdin);
		scanf_s("%f%c", &v, &d, 1);
	}
	if (d == 'D')
	{
		res = v*(3.14 / 180);
		printf("It's %.2fR \n", res);
	}
	if (d == 'R')
	{
		res = v*(180 / 3.14);
		printf("It's %.2fD \n", res);
	}
	
	//printf("d is %c\n", d);
	return 0;
}

