// 3.1Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{
    int trig = 0, words=0, sym=0;
    printf("Insert the line\n");
    while ((sym = getchar()) != '\n')
    {
        if (sym == ' ' || sym == '\t')
            trig = 0;
        else if (trig == 0) {
            trig = 1;
            ++words;
        }
    }
    printf("The number of words is %d\n", words);
    return 0;
}

