// Task2.2Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <time.h>
#define MAX 11


int _tmain(int argc, _TCHAR* argv[])
{
    srand(time(NULL));
    int secret_number = rand()%MAX;
    printf("Guess the number from 0 to 10\n");
    int guess,k;
    do
    {
        k = scanf_s("%d", &guess);
        if ((!k) || (guess < 0) || (guess>=MAX))
        {
        printf("Incorrect data! Try again\n");
        fflush(stdin);
        }
        if ((guess < secret_number) && (guess>0))
        {
          printf("The number is bigger. Try again\n");
          fflush(stdin);
          k = 0;
        }
        else if ((guess > secret_number) && (guess<MAX))
        {
          printf("The number is smaller. Try again\n");
          fflush(stdin);
          k = 0;
        }
    } while (guess != secret_number);

        printf("Congratulations!\n");

     return 0;
}

