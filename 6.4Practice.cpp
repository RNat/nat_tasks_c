// 6.4Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <math.h>
#include <stdlib.h>
#include <time.h>

int traditional(long int *buf, long int size) //traditional summing
{
    long int result = 0;
    for (int i = 0; i < size; i++)
        result+=buf[i];
    return result;
}

int recursion(long int *buf, long int size) //recursive summing
{
    if (size == 1)
        return buf[0];
    else
        return recursion(buf, size / 2) + recursion(buf + size / 2, size - size / 2);
}

int main(int argc, char* argv[])
{
    long int *buf = NULL;
    int exp = atoi(argv[1]);
    long int size = pow(2.0, exp);  //calculating the size of the array
    buf = (long int*)malloc(size*sizeof(buf));

    for (int i = 0; i < size; i++)  //filling the array with random numbers
        buf[i] = rand() % 100;

    clock_t start = clock();

    long int result_t = traditional(buf, size);

    clock_t end = clock();

    double time_t = (double)(end - start) / CLOCKS_PER_SEC; //time of traditional summing

    start = clock();

    long int result_r = recursion(buf, size);

    end = clock();

    free(buf);

    double time_r = (double)(end - start) / CLOCKS_PER_SEC;  //time of recursive summing

    printf("The result of the traditional summing is %ld\nwith recursion - %ld\n", result_t, result_r);

    if (time_t > time_r) puts("Traditional way of summing is faster");
    else if (time_t < time_r) puts("Recursion is faster");
    else puts("Both take equal time");

    return 0;
}

