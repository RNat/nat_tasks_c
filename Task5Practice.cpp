// Task5Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#include <stdlib.h>

int _tmain(int argc, _TCHAR* argv[])
{
	int MAX_WIDTH = 80;
	char    input[80];
	int     len, padding=0;
	printf("Please enter a text of max. %i chars:\n", MAX_WIDTH);
	if (fgets(input, sizeof(input), stdin) == NULL) 
		exit(1);
	len = strlen(input);
	padding = ((MAX_WIDTH - len) / 2)+len;

	printf("%*s%s", padding, input);

	return 0;
}

