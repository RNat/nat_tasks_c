// 5.1Practice.cpp : Defines the entry point for the console application.
#include "stdafx.h"
#include<string.h>
#include <stdlib.h>
#include <time.h>
#define MAX 80

void Print(char* arr)
{
    while (*arr && *arr != ' ')
        putchar(*arr++);
    putchar(' ');
}

int _tmain(int argc, _TCHAR* argv[])
{
    char string[MAX];
    char* ptr[MAX];
    printf("Enter the string\n");
    fgets(string, sizeof(string), stdin);
    int number_of_words = 0;
    string[strlen(string) - 1] = 0;

    for (int i = 0; i < strlen(string); i++)
    {
        if (string[i] != ' ' && (string[i - 1] == ' ' || (i == 0)))
            ptr[number_of_words++] = string + i;
    }

    srand(time(NULL));

    for (int i = 0; i <= number_of_words - 1; i++)
    {
        int var = rand() % number_of_words;

        char* tmp = ptr[i];
        ptr[i] = ptr[var];
        ptr[var] = tmp;
    }
    for (int i = 0; i <= number_of_words - 1; i++)
        Print(ptr[i]);
    return 0;
}