// Decompressor.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <stdlib.h>
#include <string.h>

#define MAX 512

typedef struct SYM
{
    unsigned char ch;  //ASCII code of the symbol
    //    int count;  //how many times the symbol repeats in the file
    float freq;  //frequency
    char code[MAX];
    struct SYM *left;
    struct SYM *right;
};

typedef union CODE
{
    unsigned char ch;
    struct BYTE
    {
        unsigned b1 : 1;
        unsigned b2 : 1;
        unsigned b3 : 1;
        unsigned b4 : 1;
        unsigned b5 : 1;
        unsigned b6 : 1;
        unsigned b7 : 1;
        unsigned b8 : 1;
    } byte;
};

struct SYM* buildTree(struct SYM *psym[], int N) //building binary tree
{
    struct SYM *temp = (struct SYM*)malloc(sizeof(struct SYM));
    temp->freq = psym[N - 1]->freq + psym[N - 2]->freq;
    temp->code[0] = 0;
    temp->left = psym[N - 1];
    temp->right = psym[N - 2];

    if (N == 2)
        return temp;
    else
    {
        for (int i = 0; i < N; i++)
            if (temp->freq>psym[i]->freq)
            {
            for (int j = N - 1; j > i; j--)
                psym[j] = psym[j - 1];

            psym[i] = temp;
            break;
            }
    }
    return buildTree(psym, N - 1);
}

void makeCodes(SYM *root) //creating codes
{
    if (root->left)
    {
        strcpy_s(root->left->code, root->code);
        strcat_s(root->left->code, "0");
        makeCodes(root->left);
    }
    if (root->right)
    {
        strcpy_s(root->right->code, root->code);
        strcat_s(root->right->code, "1");
        makeCodes(root->right);
    }
}

int _tmain(int argc, _TCHAR* argv[])
{
    FILE *f1, *f2, *f3, *f4;
    int alph = 0; //alphabet size
    int ts; //tail size
    SYM symbols[MAX] = { 0 }; //array of symbols
    SYM *psym[MAX]; //pointers to the array of symbols
    char s[8]; //an array for decoding
    char chh;


    fopen_s(&f1, "output.txt", "rb"); //compressed file
    fopen_s(&f2, "temp.txt", "wb"); //temporary file with code
    fopen_s(&f3, "result.txt", "wb"); //decompressed file

    fscanf_s(f1, "%d", &alph);
    fscanf_s(f1, "%d", &ts);



    for (int i = 0; i < alph; i++)
    {
        fscanf_s(f1, "%c", &symbols[i].ch);
        fscanf_s(f1, "%f", &symbols[i].freq);
        symbols[i].right = symbols[i].left = NULL;
        psym[i] = symbols + i;
        getc(f1);
    }

    //    for (int i = 0; i < alph; i++)
    //        psym[i] = &symbols[i];

    SYM *root = buildTree(psym, alph);

    makeCodes(root);
    fopen_s(&f4, "code.txt", "wb");
    for (int i = 0; i < alph; i++)
        fprintf(f4, "%c%s", symbols[i].ch, symbols[i].code);

    SYM *pnode;

    union CODE code1;


    while (!feof(f1))
    {
        chh = getc(f1);
        code1.ch = chh;
        s[0] = code1.byte.b1 + '0';
        s[1] = code1.byte.b2 + '0';
        s[2] = code1.byte.b3 + '0';
        s[3] = code1.byte.b4 + '0';
        s[4] = code1.byte.b5 + '0';
        s[5] = code1.byte.b6 + '0';
        s[6] = code1.byte.b7 + '0';
        s[7] = code1.byte.b8 + '0';
        for (int i = 0; i < 8; i++)
            fprintf(f2, "%c", s[i]);
    }

    fseek(f2, -ts, SEEK_END);
    fprintf(f2, "%x", EOF);
    fclose(f2);


    pnode = root;
    fopen_s(&f2, "temp.txt", "rb");


    while (!feof(f2))
    {
        chh = getc(f2);
        if (chh == '0')
            pnode = pnode->left;
        else
            pnode = pnode->right;
        if (pnode->left == NULL && pnode->right == NULL)
        {
            putc(pnode->ch, f3);
            pnode = root;
        }
    }

    return 0;
}

