// 6.7Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#define width 27
#define height 9
#define start_x 4  //coordinates of the starting point
#define start_y 14

char labyrinth[height][width] = { "##########################",
"#          #  #          #",
"########## #  #          #",
"#          #  #######  ###",
"# #######  #             #",
"#      ##  #  ######   ###",
"#####  ##  #  #        ###",
"       ##     #     ######",
"##########################" };


void print_labyrinth(int current_x, int current_y)
{
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            if (i == current_x && j == current_y)
                putchar('X');
            else putchar(labyrinth[i][j]);
        }
        putchar('\n');
    }
    Sleep(500);
    system("cls");
}

void searching_for_exit(int current_x, int current_y, char previous_position)
{
    if (current_y == 0 && labyrinth[current_x][current_y] == ' ')  //checking if we found the exit already
    {
        printf("We found the exit!\n");
        exit(1);
    }

    else if (labyrinth[current_x - 1][current_y] == ' ' && previous_position != 't')  //checking if the top position is free to move and if we already were there
    {
        current_x--;
        previous_position = 'b';
    }

    else if (labyrinth[current_x + 1][current_y] == ' ' && previous_position != 'b')  //checking the bottom
    {
        current_x++;
        previous_position = 't';
    }

    else if (labyrinth[current_x][current_y - 1] == ' ' && previous_position != 'l')  //checking the left position
    {
        current_y--;
        previous_position = 'r';
    }

    else if (labyrinth[current_x][current_y + 1] == ' ' && previous_position != 'r')  //checking the right
    {
        current_y++;
        previous_position = 'l';
    }

    else if (previous_position == 't')  //if the program reached this point, it means no other position is avaliable and we have no choice but to go back
    {
        current_x--;
        previous_position = 'b';
    }

    else if (previous_position == 'b')
    {
        current_x++;
        previous_position = 't';
    }

    else if (previous_position == 'l')
    {
        current_y--;
        previous_position = 'r';
    }

    else if (previous_position == 'r')
    {
        current_y++;
        previous_position = 'l';
    }

    print_labyrinth(current_x, current_y);
    searching_for_exit(current_x, current_y, previous_position);
}

int _tmain(int argc, _TCHAR* argv[])
{
    searching_for_exit(start_x, start_y, '0');
    return 0;
}

