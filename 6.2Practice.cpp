// 6.2Practice.cpp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#define beg 2
#define end 1000000

int collatz(int i, int count)    //calculating the row length for Collatz sequence
{
    if (i % 2 == 0)
    {
        i = i / 2;
        count++;
    }
    else
    {
        i = i * 3 + 1;
        count++;
    }
    if (i > 1) count = collatz(i, count);    //calculating while the number is bigger than 1
    return count;
}

int _tmain(int argc, _TCHAR* argv[])
{
    int max_count = 0, number = 0;
    for (int i = beg; i < end; i++)
    {
        int count = 0;
        count = collatz(i, count);
        if (count > max_count)
        {
            max_count = count;
            number = i;
        }
    }
    printf_s("The number is %d, the length of the row is %d\n", number, max_count);
    return 0;
}