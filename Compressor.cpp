// Compressor.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <string.h>

#define MAX 512

typedef struct SYM
{
    unsigned char ch;  //ASCII code of the symbol
    int count;  //how many times the symbol repeats in the file
    float freq;  //frequency
    char code[MAX];
    struct SYM *left;
    struct SYM *right;
};

typedef union CODE
{
    unsigned char ch;
    struct BYTE
    {
        unsigned b1 : 1;
        unsigned b2 : 1;
        unsigned b3 : 1;
        unsigned b4 : 1;
        unsigned b5 : 1;
        unsigned b6 : 1;
        unsigned b7 : 1;
        unsigned b8 : 1;
    } byte;
};

struct SYM* buildTree(struct SYM *psym[], int N) //building binary tree
{
    struct SYM *temp = (struct SYM*)malloc(sizeof(struct SYM));
    temp->freq = psym[N - 1]->freq + psym[N - 2]->freq;
    temp->code[0] = 0;
    temp->left = psym[N - 1];
    temp->right = psym[N - 2];

    if (N == 2)
        return temp;
    else
    {
        for (int i = 0; i < N; i++)
            if (temp->freq>psym[i]->freq)
            {
            for (int j = N - 1; j > i; j--)
                psym[j] = psym[j - 1];

            psym[i] = temp;
            break;
            }
    }
    return buildTree(psym, N - 1);
}

void makeCodes(SYM *root) //creating codes
{
    if (root->left)
    {
        strcpy_s(root->left->code, root->code);
        strcat_s(root->left->code, "0");
        makeCodes(root->left);
    }
    if (root->right)
    {
        strcpy_s(root->right->code, root->code);
        strcat_s(root->right->code, "1");
        makeCodes(root->right);
    }
}

int _tmain(int argc, _TCHAR* argv[])
{
    FILE *f1, *f2, *f3, *f4;
    fopen_s(&f1, "input.txt", "rb"); //sourse file
    fopen_s(&f2, "temp.txt", "wb"); //temporary file with code
    fopen_s(&f3, "output.txt", "wb"); //compressed file

    int chh; //symbols from file
    int alph = 0; //alphabet size
    int number_of_sym = 0; //number of symbols in file
    int fsize2 = 0; //number of symbols in a temporary file
    int ts; //tail size
    int value[MAX] = { 0 }; //array of numbers of every symbol
    SYM symbols[MAX] = { 0 }; //array of symbols
    SYM *psym[MAX]; //pointers to the array of symbols
    float summ = 0; //the sum of frequencies
    int mes[8];
    char j = 0;

    if (f1 == NULL)
    {
        puts("FILE NOT OPEN");
        return 0;
    }


    while ((chh = fgetc(f1)) != EOF)
    {
        for (int j = 0; j < MAX; j++)
        {
            if (chh == symbols[j].ch)
            {
                value[j]++;
                number_of_sym++;
                break;
            }
            if (symbols[j].ch == 0)
            {
                symbols[j].ch = (unsigned char)chh;
                value[j] = 1;
                alph++;
                number_of_sym++;
                break;
            }
        }
    }
    float add = 0.000001;
    //Counting frequency
    for (int i = 0; i < alph; i++)
    {
        symbols[i].freq = (float)value[i] / number_of_sym;
        add = add + 0.000001;
        symbols[i].freq = symbols[i].freq + add;
    }

    for (int i = 0; i < alph; i++)
        psym[i] = &symbols[i];

    //sorting
    SYM tempp;
    for (int i = 1; i < alph; i++)
        for (int j = 0; j < alph - 1; j++)
            if (symbols[j].freq < symbols[j + 1].freq)
            {
        tempp = symbols[j];
        symbols[j] = symbols[j + 1];
        symbols[j + 1] = tempp;
            }

    for (int i = 0; i < alph; i++)
        summ += symbols[i].freq;

    SYM *root = buildTree(psym, alph);
    makeCodes(root);
    rewind(f1);

    fopen_s(&f4, "code.txt", "wb");

    while ((chh = fgetc(f1)) != EOF)
    {
        for (int i = 0; i < alph; i++)
            if (chh == symbols[i].ch)
                fputs(symbols[i].code, f2);
    }
    fclose(f2);



    fopen_s(&f2, "temp.txt", "rb");

    while ((chh = fgetc(f2)) != EOF)
        fsize2++;
    ts = fsize2 % 8;


    //    fprintf(f3, "compress");
    fprintf(f3, "%d ", alph);
    fprintf(f3, "%d", ts);

    for (int i = 0; i < alph; i++)
    {
        fprintf(f3, "%c%.6f#", symbols[i].ch, symbols[i].freq);
        fprintf(f4, "%c%s", symbols[i].ch, symbols[i].code);
    }

    rewind(f2);

    union CODE code1;

    j = 0;
    for (int i = 0; i < fsize2 - ts; i++)
    {
        mes[j] = fgetc(f2);
        if (j == 7)
        {
            code1.byte.b1 = mes[0] - '0';
            code1.byte.b2 = mes[1] - '0';
            code1.byte.b3 = mes[2] - '0';
            code1.byte.b4 = mes[3] - '0';
            code1.byte.b5 = mes[4] - '0';
            code1.byte.b6 = mes[5] - '0';
            code1.byte.b7 = mes[6] - '0';
            code1.byte.b8 = mes[7] - '0';
            fputc(code1.ch, f3);
            j = 0;
            continue;
        }
        j++;
    }

    for (int i = 0; i < 8; i++)
        mes[i] = '0';

    j = 0;
    for (int i = 0; i <= ts; i++)
    {
        mes[j] = fgetc(f2);
        if (j == ts)
        {
            code1.byte.b1 = mes[0] - '0';
            code1.byte.b2 = mes[1] - '0';
            code1.byte.b3 = mes[2] - '0';
            code1.byte.b4 = mes[3] - '0';
            code1.byte.b5 = mes[4] - '0';
            code1.byte.b6 = mes[5] - '0';
            code1.byte.b7 = mes[6] - '0';
            code1.byte.b8 = mes[7] - '0';
            fputc(code1.ch, f3);
        }
        j++;
    }
    _fcloseall();
    return 0;
}


