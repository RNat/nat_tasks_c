// 6.8Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#include <stdlib.h>
#include <string.h>
#define max 80

char partition(char *buf, char *expr1, char *expr2)  //this function splits the string to the first and second operands and the sign of operation
{
    int left = 0, right = 0, count1 = 1;
    char operation;
    int len = strlen(buf);
    buf[len - 1] = '\0';
    while (buf)
    {
        if (buf[count1] == '(')
            left++;
        if (buf[count1] == ')')
            right++;
        if (left == right)
            break;
        count1++;    //counting the symbols for the first expression
    }

    for (int i = 1; i <= count1; i++)
        expr1[i - 1] = buf[i];
    expr1[count1] = '\0';

    operation = buf[count1 + 1];

    int j, count2;  //counting the symbols for the second expression
    for (j = count1 + 2, count2 = 0; buf[j] != '\0'; j++)
        expr2[count2++] = buf[j];
    expr2[count2] = '\0';

    return operation;  //returning the sign of operation
}

int eval(char *buf)    //this fuction calculates the result
{
    char expr1[max], expr2[max];
    if (*buf != '(')
        return atoi(buf);
    char operation = partition(buf, expr1, expr2);
    switch (operation)
    {
    case '+':
        return eval(expr1) + eval(expr2);
    case '-':
        return eval(expr1) - eval(expr2);
    case '*':
        return eval(expr1)*eval(expr2);
    case '/':
        return eval(expr1) / eval(expr2);
    }
}

int main(int argc, char* argv[])
{
    int res = eval(argv[1]);
    printf("Result is %d\n", res);
    return 0;
}