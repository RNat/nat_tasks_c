// 4.3Practice.cpp : Defines the entry point for the console application.
#include "stdafx.h"
#include<string.h>
#define MAX 80

bool check(char *arr)
{
    char *beg = arr;
    char *end = arr + strlen(arr) - 1;

    while (beg < end)
    {
        if (*beg != *end)
            return false;
        else
        {
            beg++;
            end--;
        }
    }
    return true;
}

int _tmain(int argc, _TCHAR* argv[])
{
    char string[MAX];
    printf("Enter the string\n");
    fgets(string, sizeof(string), stdin);
    string[strlen(string) - 1] = 0;
    bool res = check(string);

    if (res == false) printf("The string isn't a palindrome\n");
    else printf("The string is a palindrome\n");

    return 0;
}