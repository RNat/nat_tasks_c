// 4.5Practice.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include<string.h>
#define MAX 80

void Sorting(char** arr, int number_of_strings)
{
    for (int i = 1; i < number_of_strings; ++i) // ���� ��������, i - ����� �������
    {
        char* tmp = arr[i];
        int size = strlen(arr[i]), j;
        for (j = i - 1; j >= 0 && strlen(arr[j]) > size; --j) // ����� ����� �������� � ������� ������������������ 
            arr[j + 1] = arr[j];    // �������� ������� �������, ���� �� �����
        arr[j + 1] = tmp; // ����� �������, �������� �������    
    }
}

void Print(char** arr, int number_of_strings, FILE* out)
{
    for (int i = 0; i<number_of_strings; i++)
        fprintf(out, "%s\n", arr[i]);
}

int _tmain(int argc, _TCHAR* argv[])
{
    char strings[MAX][MAX];
    char* ptr[MAX];
    FILE* in;
    fopen_s(&in, "input.txt", "r");
    FILE* out;
    fopen_s(&out, "output.txt", "w");
    int i = 0, number_of_strings = 0;
    char* read = 0;
    do
    {
      read = fgets(strings[i], sizeof(strings[i]), in);
        ptr[i] = strings[i];
        i++;
        number_of_strings++;
    } while (read != NULL);

    Sorting(ptr, number_of_strings);
    Print(ptr, number_of_strings, out);
    fclose(in);
    fclose(out);
    return 0;
}