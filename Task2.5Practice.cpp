// Task2.5Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define LEN 8


int _tmain(int argc, _TCHAR* argv[])
{
    char pass[LEN];
    srand(time(NULL));
    for (int i = 0; i<10; i++)
    {
        int counta=0, countB=0, count1=0;
        for (int k = 0; k<LEN; k++)
        {
            int choose = rand() % 3;
            switch (choose)
            {
            case 0:
                pass[k]=rand() % ('z' - 'a') + 'a';
                counta++;
                break;
            case 1:
                pass[k]=rand() % ('Z' - 'A') + 'A';
                countB++;
                break;
            default:
                pass[k]=rand() % ('9' - '0') + '0';
                count1++;
            }
        }

        if ((counta) && (countB) && (count1))
            printf("%s\n", pass);
        else i--;
        fflush(stdout);
    }
    return 0;
}

