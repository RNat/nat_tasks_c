// 5.4Practice.cpp : Defines the entry point for the console application.
#include "stdafx.h"
#include<string.h>
#include <stdlib.h>
#include <time.h>
#define MAX 80

int WLength(char *ptr)
{
    int length = 0;
    while (*ptr && *ptr != ' ')
    {
        length++;
        ptr++;
    }
    return length;
}

int _tmain(int argc, _TCHAR* argv[])
{
    char string[MAX];
    char* ptr[MAX];

    FILE* in;
    fopen_s(&in, "input.txt", "r");
    FILE* out;
    fopen_s(&out, "output.txt", "w");

    while (fgets(string, sizeof(string), in))
    {
        int number_of_words = 0;
        if (string[strlen(string)-1] == '\n') string[strlen(string) - 1] = '\0';

        for (int i = 0; i < strlen(string); i++)
        {
            if (string[i] != ' ' && (string[i - 1] == ' ' || (i == 0)))
                ptr[number_of_words++] = string + i;
        }

        srand(time(NULL));

        for (int i = 0; i <= number_of_words - 1; i++)
        {
            int var = rand() % number_of_words;

            char* tmp = ptr[i];
            ptr[i] = ptr[var];
            ptr[var] = tmp;
        }

        for (int i = 0; i <= number_of_words - 1; i++)
            fprintf(out, "%.*s ", WLength(ptr[i]) + 1, ptr[i]);

        fprintf(out, "\n");
    }
    return 0;
}