// 3.10Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#include <conio.h>
#define MAX_SIZE 255

int ParseEven(char str[], int word)
{
    char s[MAX_SIZE];
    int count = 1;
    char *next_token = NULL;
    s[0] = '\0';
    char *token = strtok_s(str, "!;:,.?- ", &next_token);
    while (token != NULL)
    {
        if (count!=word)
        { 
            strcat_s(s, MAX_SIZE, token); 
            strcat_s(s, MAX_SIZE, " "); 
        }
        token = strtok_s(NULL, "`!;:,.?- ", &next_token);
        ++count;
    }
    if ((word<1) || (word>count))
    {
        printf("Wrong number!\n");
        return 1;
    }
    else
    {
        strcpy_s(str, MAX_SIZE, s);
        return 0;
    }
}

int _tmain(int argc, _TCHAR* argv[])
{
    char str[MAX_SIZE];
    int word;
    printf("Please enter a string\n");
    fgets(str, sizeof(str), stdin);
    puts("Enter a number of word: ");
    scanf_s("%d", &word, 2);
    int res=ParseEven(str, word);
    if (res==0)
    printf("%s", str);

    return 0;
}
