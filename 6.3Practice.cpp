// 6.3Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<string.h>
#define max 80

char convert(int digit)
{
    char tmp;
    tmp = digit + '0';
    return tmp;
}

int split(int number, char *buf, int &i, int &count)
{
    if (number > 9)
    {
        int digit = number % 10;    //calculating the remainder of division, which would be an element of an array
        buf[i++] = convert(digit);
        number = number / 10;    //the result of division
        split(number, buf, i, count);
        count++;
    }
    else
    {
        buf[i] = convert(number);
        count++;
    }
    return count;  //calculating the amount of digits in the number
}


int _tmain(int argc, _TCHAR* argv[])
{
    int number;
    char buf[max];
    puts("Enter a number: ");
    scanf_s("%d", &number, max);
    int i = 0, count = 0;
    count = split(number, buf, i, count);
    puts("Your number is: ");
    for (i = count - 1; i >= 0; i--)
        putchar(buf[i]);
    putchar('\n');
    return 0;
}

