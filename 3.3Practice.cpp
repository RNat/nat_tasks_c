// 3.3Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#include <ctype.h>
#define MAX 80

int _tmain(int argc, _TCHAR* argv[])
{
    char string[MAX], buff[MAX];
    printf("Please enter a string\n");
    fgets(string, sizeof(string), stdin);
    char *ptr = string;
    int max = 0, count = 0;
    while (*ptr)
    {
        if (isalpha(*ptr))
        {
            count++;
        }
        else
        {
            if (count > max)
            {
                max = count;
                strcpy_s(buff, ptr - count);
                buff[count] = '\0';

            }
            count = 0;
        }
        ptr++;
    }


    printf("%s\nlength is %d\n", buff, max);
    return 0;
}

