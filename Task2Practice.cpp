// Task2Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{
	printf("What is the time now? \n");
	int h, m, s;
	scanf_s("%d:%d:%d", &h, &m, &s);
	while ((h < 0) || (h>24) || (m < 0) || (m>59) || (s < 0) || (s>59))
	{
		printf("Incorrect data! \n");
		fflush(stdin);
		scanf_s("%d:%d:%d", &h, &m, &s);
	}
	if (h < 5) printf("Good night!\n");
	if ((h>=5)&&(h<12)) printf("Good morning!\n");
	if ((h >= 12)&&(h<17)) printf("Good afternoon!\n");
	if (h >= 17) printf("Good evening!\n");
	return 0;
}

