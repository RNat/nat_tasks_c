// 3.5Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <time.h>
#define N 10

int _tmain(int argc, _TCHAR* argv[])
{
    int buf[N] = { 0 };
    int sum = 0, max=0, min=0, beg, end;
    printf("Array is: \n");
    srand(time(NULL));

    for (int i = 0; i < N; i++)
    {
        buf[i] = rand() % 10;
        if (buf[i]>buf[max])
            max = i;
        if (buf[i] < buf[min])
            min = i;
        printf("%d  ", buf[i]);
    }

    if (min < max)
    {
        beg = min,
        end = max;
    }
    else
    {
        beg = max;
        end = min;
    }
    {
        for (int i = beg; i <= end; i++)
            sum += buf[i];
        printf("\nThe sum of numbers is %d\n", sum);
    }
    return 0;
}