// 3.2Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#define LENGTH 80

int _tmain(int argc, _TCHAR* argv[])
{
    char word[LENGTH];
    int doLoop = 1, i = 0, j;
    do {
        word[i] = getchar();
        if (word[i] == ' ' || word[i] == '\n')
        {
            doLoop = word[i] != '\n';
                for (j = 0; j < i; j++)
                    putchar(word[j]);
              printf(" %i\n", i);
              i = 0;
        }
        else
            i++;
    } while (doLoop);
return 0;
}

