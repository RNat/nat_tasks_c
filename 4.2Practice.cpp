// 4.2Practice.cpp : Defines the entry point for the console application.
#include "stdafx.h"
#include<string.h>
#define MAX 80

void Print(char* arr)
{
    while (*arr && *arr != ' ')
        putchar(*arr++);
    putchar(' ');
}

int _tmain(int argc, _TCHAR* argv[])
{
    char string[MAX];
    char* ptr[MAX];
    printf("Enter the string\n");
    fgets(string, sizeof(string), stdin);
    int number_of_words = 0;
    string[strlen(string) - 1] = 0;

    for (int i = 0; i < strlen(string); i++)
    {
        if (string[i] != ' ' && (string[i-1] == ' '|| (i==0)))
            ptr[number_of_words++] = string + i;
    }

    for (int i = number_of_words - 1; i >= 0; i--)
    Print(ptr[i]);

    return 0;
}