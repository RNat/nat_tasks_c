// 3.4Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <ctype.h>
#include <string.h>

#define MAX 80
#define STEP 3

int number(char* str, int* i)
{
    int count = 0, sum = 0;
    do
    {
        sum *= 10;
        sum += str[*i] - '0';
        count++;
        (*i)++;
    } while ((isdigit(str[*i])) && (count<STEP));
    return sum;
}

int _tmain(int argc, _TCHAR* argv[])
{
    char str[MAX];
    int result = 0;
    int i = 0;
    puts("Please enter a string");
    fgets(str, sizeof(str), stdin);
    str[strlen(str) - 1] = 0;

    while (str[i])
    {
        if (isdigit(str[i])) 
        {
            result += number(str, &i);
        }
        else
            i++;
    }
    printf("The sum of numbers is %d\n", result);
    return 0;
}

