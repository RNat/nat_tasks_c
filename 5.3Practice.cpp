// 5.3Practice.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<stdlib.h>
#include<string.h>
#include<time.h>
#define MAX 80

int Words(char** ptr, char* string)
{
    int number_of_words = 0;
    for (int i = 0; i < strlen(string); i++)
    {
        if (string[i] != ' ' && (string[i - 1] == ' ' || (i == 0)))
            ptr[number_of_words++] = string + i;
    }

    return number_of_words;
}

int WLength(char *ptr)
{
    int length = 0;
    while (*ptr && *ptr != ' ')
    {
        length++;
        ptr++;
    }
    return length;
}

void Mix(char* ptr, int length)
{
    srand(time(NULL));
    if (length > 2)
    {
        for (int i = 1; i < length - 1; i++)
        {
            int var = rand() % (length - 2) + 1;
            char temp = *(ptr + i);
            *(ptr + i) = *(ptr + var);
            *(ptr + var) = temp;
        }
    }
    else return;
}

int _tmain(int argc, _TCHAR* argv[])
{
    char string[MAX];
    char* ptr[MAX];
    FILE* in;
    fopen_s(&in, "input.txt", "r");
    FILE* out;
    fopen_s(&out, "output.txt", "w");

    while (fgets(string, sizeof(string), in))
    {
        string[strlen(string) - 1] = 0;
        int number_of_words = Words(ptr, string);
        for (int i = 0; i < number_of_words; i++)
        {
            int length = WLength(ptr[i]);
            Mix(ptr[i], length);
            fprintf(out, "%.*s", length + 1, ptr[i]);
        }
        fprintf(out, "\n");
    }
    fclose(in);
    fclose(out);
    return 0;
}


