// 4.4Practice.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <string.h>
#include <stdlib.h>
#define MAX 256

int main()
{
    char buf[MAX];
    printf("Insert the line\n");
    fgets(buf, sizeof(buf), stdin);
    int length = strlen(buf), number = 0, max_number = 0;
    char *ptr = buf;

        buf[length - 1] = '\0';

        for (int i = 0; i < length; i++)
        {
            if (buf[i] == buf[i + 1])
                number++;
            if (number > max_number)
            {
                max_number = number;
                ptr = &buf[i+1];
            }
            else number = 0;
        }

        if (max_number > 1)
        {
            printf("%d - ", max_number + 1);
           // *ptr++;
            for (int i = 0; i < max_number + 1; i++) putchar(*ptr--);

            printf("\n");
        }

        else printf("There're no repeatings of the same symbol in a row\n");
    return 0;
}
